;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Master Key Editor Configuration 
;;   ____                     
;;  /    \_______________      
;; (  /\                 |      
;; (  \/  ___________   _|       
;;  \____/           |_|          
;;                                 
;; Emacs Server with Vi editing behaviour
;; Together in unholy matrimony
;;

(add-to-list 'load-path "~/.emacs.d/core")

(require 'configuration-utilities)
(require 'ui-configuration)
(package-initialize) ;; duplicate, will be put back in init.el by package.el if missing here
(bootstrap-package-manager)
(set-backup-directories)
(load-radiant-theme)
(setq custom-file "~/.emacs.d/custom.el")

(use-package paredit
  :ensure t
  :config
  (progn
    (define-key
      paredit-mode-map
      (kbd "M-k")
      'paredit-forward-slurp-sexp)))

(require 'code-mode-configuration)
(require 'file-navigation)
(use-package ag :ensure t)

(require 'clipboard)
(require 'notes-mode)

(if (eq system-type 'darwin)
    (use-package exec-path-from-shell
      :ensure t
      :config (exec-path-from-shell-initialize)))

(require 'vi-editing)
