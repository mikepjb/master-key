(defun assign-filetype (extension mode)
  (add-to-list 'auto-mode-alist '((concat "\\" extension "\\'") . mode)))

;;;###autoload
(defun load-radiant-theme ()
  (add-to-list 'custom-theme-load-path "~/.emacs.d/colors")
  (load-theme 'radiant t))

;;;###autoload
(defmacro aif (&rest forms)
    "Create an anonymous interactive function.
    Mainly for use when binding a key to a non-interactive function."
    `(lambda () (interactive) ,@forms))

;;;###autoload
(defun set-backup-directories ()
  (progn
    (setq backup-directory-alist
          `((".*" . ,temporary-file-directory)))
    (setq auto-save-file-name-transforms
          `((".*" ,temporary-file-directory t)))))

;;;###autoload
(defun bootstrap-package-manager ()
  (progn
    (require 'package)
    (package-initialize)
    (add-to-list 'package-archives `("melpha" . "http://melpa.org/packages/"))
    (require 'use-package)))

(defun fill-keymap (keymap &rest mappings)
  "Fill `KEYMAP' with `MAPPINGS'.
See `pour-mappings-to'."
  (declare (indent defun))
  (pour-mappings-to keymap mappings))

(defun pour-mappings-to (map mappings)
  "Calls `mikepjb/set-key' with `map' on every key-fun pair in `MAPPINGS'.
`MAPPINGS' is a list of string-fun pairs, with a `READ-KBD-MACRO'-readable string and a interactive-fun."
  (dolist (mapping (group mappings 2))
    (mikepjb/set-key map (car mapping) (cadr mapping)))
  map)

(defun group (lst n)
  "Group `LST' into portions of `N'."
  (let (groups)
    (while lst
      (push (take n lst) groups)
      (setq lst (nthcdr n lst)))
    (nreverse groups)))

(defun take (n lst)
  "Return atmost the first `N' items of `LST'."
  (let (acc '())
    (while (and lst (> n 0))
      (cl-decf n)
      (push (car lst) acc)
      (setq  lst (cdr lst)))
    (nreverse acc)))

(defun mikepjb/set-key (map spec cmd)
  "Set in `map' `spec' to `cmd'.
`Map' may be `'global' `'local' or a keymap.
A `spec' can be a `read-kbd-macro'-readable string or a vector."
  (let ((setter-fun (cl-case map
                      (global #'global-set-key)
                      (local  #'local-set-key)
                      (t      (lambda (key def) (define-key map key def)))))
        (key (cl-typecase spec
               (vector spec)
               (string (read-kbd-macro spec))
               (t (error "wrong argument")))))
    (funcall setter-fun key cmd)))

(provide 'configuration-utilities)
