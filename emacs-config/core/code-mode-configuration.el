(use-package inf-ruby :ensure t)

(use-package clojure-mode
  :ensure t
  :init
  (use-package cider
    :ensure t
    :config (setq cider-repl-display-help-banner nil))
  :config
  (progn
    ;; XXX correct config-utils function to assign filetypes here
    (add-to-list 'auto-mode-alist '("\\.clj\\'" . clojure-mode))
    (add-to-list 'auto-mode-alist '("\\.cljs\\'" . clojurescript-mode))))

(use-package web-mode
  :ensure t
  :config (progn
            (add-to-list 'auto-mode-alist '("\\.page\\'" . web-mode))))

(use-package ess :ensure t :defer t)
(use-package git-commit :ensure t)

(add-to-list 'auto-mode-alist '("bashrc" . sh-mode))
(add-hook 'clojure-mode-hook 'paredit-mode)
(add-hook 'emacs-lisp-mode-hook 'paredit-mode)
(add-hook 'cider-repl-mode-hook 'paredit-mode)

(provide 'code-mode-configuration)
