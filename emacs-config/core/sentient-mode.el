(setq sentient-highlights
      '(("function" . font-lock-function-name-face)
        ("bool" . font-lock-type-face)
        ("int[0-9]+" . font-lock-type-face)
        ("array[0-9]+" . font-lock-type-face)
        ("(true|false)" . font-lock-doc-face)
        ("\{" . font-lock-function-name-face)
        ("\}" . font-lock-function-name-face)
        (";" . font-lock-string-face)
        ("*[a-z]*.?" . font-lock-function-name-face)

        ))

(defun sentient-mode-variables ()
  (setq-local comment-start "#"))

(define-derived-mode sentient-mode prog-mode "Sentient"
  "Major mode for editing Sentient code."

  (setq font-lock-defaults '(sentient-highlights))
  (sentient-mode-variables))
