(use-package ivy
  :ensure t
  :config
  (progn
    (use-package flx
      :ensure t
      :init
      ;; XXX warning - fuzzy search grinds to a halt in large codebases
      (setq ivy-re-builders-alist
            '((ivy-switch-buffer . ivy--regex-plus)
              (t . ivy--regex-fuzzy))))
    (ivy-mode)))

(use-package projectile
  :ensure t
  :config
  (progn (setq-default projectile-completion-system 'ivy)
         (use-package counsel :ensure t)
         (use-package counsel-projectile :ensure t)))

(provide 'file-navigation)
