(menu-bar-mode -1)

(if (display-graphic-p)
    (progn
      (scroll-bar-mode -1)
      (tool-bar-mode -1)
      (blink-cursor-mode -1)
      (tooltip-mode nil)
      (if (eq system-type 'darwin)
          (set-frame-font "Inconsolata 18")
        (set-frame-font "Inconsolata Bold 19"))))

(show-paren-mode 1)
(column-number-mode 1)

(setq scroll-step            1
      scroll-conservatively  10000)

(setq backup-inhibited t
      make-backup-files nil
      auto-save-default nil
      inhibit-startup-message t
      ns-use-native-fullscreen nil)

(setq-default indent-tabs-mode nil)
(setq vc-follow-symlinks t) ;; do not ask when opening symlink
(defalias 'yes-or-no-p 'y-or-n-p)

(global-set-key (kbd "M-RET") 'toggle-frame-fullscreen)
(global-set-key (kbd "C-q") (aif (counsel-find-file "~/code/")))
(global-set-key (kbd "C-w") 'backward-kill-word)
(global-set-key (kbd "C-;") 'hippie-expand)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "M-/") 'comment-or-uncomment-region)

(provide 'ui-configuration)
