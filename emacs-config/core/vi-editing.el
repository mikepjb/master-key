(use-package evil
  :ensure t
  :config
  (progn
    (evil-mode 1)
    (if (display-graphic-p)
        (define-key evil-normal-state-map (kbd "C-z") 'ansi-term)
      (define-key evil-normal-state-map (kbd "C-z") 'suspend-frame))
    (fill-keymap evil-normal-state-map
      (kbd "C-a") 'evil-beginning-of-line
      (kbd "C-e") 'end-of-line
      (kbd "C-s") 'save-buffer)
    (fill-keymap evil-insert-state-map
      (kbd "C-s") (aif (save-buffer) (evil-normal-state))
      (kbd "C-f") 'forward-char
      (kbd "C-b") 'backward-char
      (kbd "C-k") (aif (insert " => "))
      (kbd "C-h") 'delete-backward-char
      (kbd "C-a") 'evil-beginning-of-line
      (kbd "C-e") 'end-of-line
      (kbd "C-c") 'evil-normal-state)))

(use-package evil-leader
  :ensure t
  :init (progn 
          (global-evil-leader-mode)
          (evil-leader/set-leader "<SPC>")
          (evil-leader/set-key
            "e" 'find-file
            "f" 'counsel-projectile)))

(dolist
    (mode-map '((cider-repl-mode . emacs)
                (special-mode . emacs)
                (inf-ruby-mode . emacs)))
  (evil-set-initial-state `,(car mode-map) `,(cdr mode-map)))

(provide 'vi-editing)
