;;;###autoload
(defun xsel-paste-function ()
  (let ((xsel-output (shell-command-to-string "xsel --clipboard --output")))
    (unless (string= (car kill-ring) xsel-output)
      xsel-output)))

;;;###autoload
(defun xsel-cut-function (text &optional push)
  (with-temp-buffer
    (insert text)
    (call-process-region (point-min) (point-max)
      "xsel" nil 0 nil "--clipboard" "--input")))

;;;###autoload
(defun osx-clipboard-cut-function (text &rest ignore)
    "Copy TEXT to the OS X clipboard using \"pbpaste\".
This is set as the value of `interprogram-cut-function' by
`osx-clipboard-mode'.  It should only be used when Emacs is running in a
text terminal."
    (with-temp-buffer
      (insert text)
      (with-demoted-errors "Error calling pbcopy: %S"
	(call-process-region (point-min) (point-max) "pbcopy"))))

;;;###autoload
(defun osx-clipboard-paste-function ()
  "Return the value of the OS X clipboard using \"pbcopy\".
This is set as the value of `interprogram-paste-function' by
`osx-clipboard-mode'.  It should only be used when Emacs is running in a
text terminal."
  (with-temp-buffer
    (with-demoted-errors "Error calling pbpaste: %S"
      (call-process "pbpaste" nil t)
      (let ((text (buffer-substring-no-properties (point-min) (point-max))))
  ;; The following logic is adapted from `x-selection-value'
  ;; in `ns-win.el.gz'
  (cond
    ((or
       ;; Avoid copying an empty clipboard, or copying the same
       ;; text twice
       (not text)
       (eq text osx-clipboard-last-selected-text)
       (string= text "")
       (string= text (car kill-ring))) nil)
    ((string= text osx-clipboard-last-selected-text)
     ;; Record the newer string, so subsequent calls can use the `eq' test.
     (setq osx-clipboard-last-selected-text text)
     nil)
    (t
      (setq osx-clipboard-last-selected-text text)))))))

(cond
 ((eq system-type 'darwin)
  (progn (defvar osx-clipboard-last-selected-text nil)
         (setq interprogram-cut-function 'osx-clipboard-cut-function
               interprogram-paste-function 'osx-clipboard-paste-function)))
  ((eq system-type 'gnu/linux)
  (progn
    (setq x-select-enable-clipboard t)
    (setq interprogram-cut-function 'xsel-cut-function
          interprogram-paste-function 'xsel-paste-function))))

(provide 'clipboard)
