;; notes.el mikepjb notes plugin

(setq my-highlights
      '(("^o*$" . font-lock-function-name-face)
        ("^X" . font-lock-comment-face)
        ("^-.* " . font-lock-doc-face)
        ("^> .*$" . font-lock-function-name-face)
        ("^https?:\/\/.*$" . font-lock-constant-face)
        ("^\* .*$" . font-lock-type-face)
        ("^\*\* .*$" . font-lock-constant-face)
        ))

(setq-local comment-start "{{{")
(setq-local comment-end "}}}")

;; (define-derived-mode notes-mode shell-mode "notes")

(define-derived-mode notes-mode prog-mode
  (setq font-lock-defaults '(my-highlights))
  (setq mode-name "notes")
  (setq-local comment-start "# ")
  (setq-local comment-start-skip "#+[\t ]*"))

(add-to-list 'auto-mode-alist '("\\.notes\\'" . notes-mode))

(provide 'notes-mode)
