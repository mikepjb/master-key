# masterkey bashrc

export PLATFORM=$(uname -s)
[ -f /etc/bashrc ] && . /etc/bashrc
[ -f /etc/bash_completion ] && . /etc/bash_completion

shopt -s histappend # Append to the history file
shopt -s checkwinsize # Check the window size after each command
shopt -s nocaseglob #case insensitive completion
[[ $- =~ i ]] && stty -ixoff -ixon # Disable CTRL-S and CTRL-Q

bind '"\C-g":" em $(find ~/notes/* -type f | selecta)\n"'
bind '"\C-q":" cd ~/code/$(find ~/code/* -maxdepth 0 -type d -printf \"%f\n\"| selecta)\n"'

export LANG=en_US.UTF-8
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=
export HISTFILESIZE=
export EDITOR=em
[ -z "$TMPDIR" ] && TMPDIR=/tmp

if [ -z "$PATH_EXPANDED" ]; then
    export GOPATH=~/.gosrc && mkdir -p $GOPATH
    if [ "$PLATFORM" == Darwin ]; then
        # XXX emacsclient binary is here on OSX when compiled from source --with-ns
        export PATH=/Applications/Emacs.app/Contents/MacOS/bin/:$PATH
    fi
    export PATH=~/master-key/bin:/opt/bin:/usr/local/bin:/usr/local/share/python:/usr/local/opt/go/libexec/bin:$PATH
    export PATH_EXPANDED=1
fi

source /usr/local/share/chruby/chruby.sh
source /usr/local/share/chruby/auto.sh

alias tags='ctags -R $(git rev-parse --show-toplevel || echo ".")'
alias space='df -h'
alias .space='du -h'
alias l='ls -alF'
alias pgstart='sudo systemctl start postgresql'
alias brd='nohup boot repl -p 9999 -s wait &> /dev/null &'
alias lrd='$(nohup lein repl :headless :port 9999 0>&- &>/dev/null &)'
alias lrc='lein repl :connect 9999'
alias lrx='tear-down-repls'
alias lra="ps ex | ag 'lein.*.repl' | grep -v 'ag lein' | cut -d ' ' -f1"
alias ts0="printf '\e[8;50;100t'"
alias ts1="printf '\e[8;50;160t'"
alias ts2="printf '\e[8;20;100t'"
alias json="python -m json.tool"
alias ..='cd ..'
alias ...='cd ../..'
alias grt='cd $(git rev-parse --show-toplevel || echo ".")'
alias t='tmux attach -t vty || tmux new -s vty'

if [ "$PLATFORM" == Darwin ]; then
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.:/usr/local/lib
    export JAVA_HOME='/Library/Java/JavaVirtualMachines/jdk1.8.0_60.jdk/Contents/Home'
    export COPYFILE_DISABLE=true
    alias ls='ls -G'
    export LSCOLORS="dxgxcxdxexegedabagacad"
    alias acpi="pmset -g batt"
    alias ctags="`brew --prefix`/bin/ctags"
    alias pgstart='pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start'
else
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias screenshot=xscrot
    export LS_COLORS='di=33:ln=36:so=32:pi=33:ex=34:bd=34;46:cd=34;43:su=0;41:sg=0;46:tw=0;42:ow=0;43:'
fi

RED="\[\e[0;31m\]"
GREEN="\[\e[0;32m\]"
YELLOW="\[\e[0;33m\]"
BLUE="\[\e[34m\]"
MAGENTA="\[\e[35m\]"
CYAN="\[\e[36m\]"
NORMAL="\[\033[m\]"

git_branch() {
    echo -e "$(git branch 2>/dev/null| sed -n '/^\*/s/^\* //p')"
}

git_state() {
    if git rev-parse --git-dir >/dev/null 2>&1; then
        if git diff --quiet 2>/dev/null >&2; then
            color=$GREEN
        else
            color=$RED
        fi
        echo -ne "${color}$(git_branch)${NORMAL}"
    else
        echo -ne "${CYAN}!${NORMAL}"
    fi
}

PROMPT_COMMAND='PS1="\W($(git_state)) $CYAN\$$NORMAL "'

ew() {
    em `which "$1"`
}

activate_virtualenv() {
    if [ -f env/bin/activate ]; then . env/bin/activate;
    elif [ -f ../env/bin/activate ]; then . ../env/bin/activate;
    elif [ -f ../../env/bin/activate ]; then . ../../env/bin/activate;
    elif [ -f ../../../env/bin/activate ]; then . ../../../env/bin/activate;
    elif [ -f ~/toolkit/env ]; then . ~/toolkit/env/bin/activate;
    fi
}

